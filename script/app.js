// modules and binding
// for modules
//FOR EXPRESSIONS
angular.module('app',[]).controller("MainController",function(){
	this.num1 = 0;
	this.num2 = 0;
});
// this is a angular app using a java class
// name the module (app) and give it an empty array, 
	// the array could be empty or hold some other stuff
// we create a controller that takes two args, the name (MainController) and a closure (function) w. properties and methods
angular.module('appIndex',[]).controller("MainControllerIndex",function(){
	this.name = "World";
});

angular.module('appRepeat',[]).controller("MainControllerRepeat",function(){
	this.tasks = [
		{
			name: "Go to grocery",
			done: false
		},
		{
			name:"Walk the dog",
			done: false
		},
		{
			name: "Dinner with boss",
			done: false
		}
	];

	this.add = function(newtask){
		console.log("adding task: "+ newtask.name);
		var task = {};
		// task is a new empty object
		task.name = newtask.name;
		// name is key, newtask.name is value
		task.done = false;
		this.tasks.push(task);
		newtask.name = "";
		// blanks after submission
	};

	this.delete = function(task){
		console.log("Deleting task: " + task.name);
		for (var i=0; i<this.tasks.length; i++){
			if (this.tasks[i].name == task.name){
				this.tasks.splice(i,1);
				break;
			}
		}
	};
});